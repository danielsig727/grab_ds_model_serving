FROM tensorflow/serving:latest as tfserving_src

###
FROM python:3.7-buster

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -y --no-install-recommends supervisor ca-certificates && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /root

# tf-serving ref: https://github.com/tensorflow/serving/blob/master/tensorflow_serving/tools/docker/Dockerfile
COPY --from=tfserving_src /usr/bin/tensorflow_model_server /usr/bin/tensorflow_model_server

ENV MODEL_BASE_PATH=/models
RUN mkdir -p ${MODEL_BASE_PATH}

ARG MODEL_NAME=example_model
ENV MODEL_NAME=$MODEL_NAME

ADD start_tf_serving.sh /root/start_tf_serving.sh

# install the server proxy
COPY poetry.lock pyproject.toml /root/
COPY grabds_serving/ /root/grabds_serving

ENV POETRY_VIRTUALENVS_CREATE=false

RUN pip install poetry==1.0.0
RUN poetry install -vvv
ADD start_webserver.sh /root

# set up supervisord
RUN mkdir -p /var/log/supervisord
COPY supervisord.conf /root/supervisord.conf
CMD ["/usr/bin/supervisord", "-c", "/root/supervisord.conf"]

# add variables and model
ENV PORT=8080
EXPOSE $PORT

ADD models /models
