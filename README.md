# Grab DS Model Server

Author: Shao-Heng Tai <danielsig727@gmail.com>

## Overview

This is a model server for the Grab DS take-home assignment.
It utilizes a tensorflow-serving as inference backend, and an aiohttp server in
`grabds_serving/` for reverse proxy and input validation.

An pre-built image is ready at `registry.gitlab.com/danielsig727/grab_ds_model_serving:latest`.

Also, it can be deployed using Cloud Run:

[![Run on Google Cloud](https://deploy.cloud.run/button.svg)](https://deploy.cloud.run?git_repo=https://bitbucket.org/danielsig727/grab_ds_model_serving.git)

## Packaging a new model

1. Add the model folder (in `tf.SavedModel` format) into `models/` in format:
   `models/<model_name>/1`. And the original `example-model` can be removed.
2. Rebuild the docker image: `env IMAGE=<image-name> MODEL_NAME=<model_name> ./build.sh`
3. The image is built as `<image-name>`. It can be used to deploy. The default serving port is 8080.

## Running the server

The server by default serves at port 8080, run it as:

    docker run -it -p 8080:8080 <image-name>

## Accessing the server

The inference endpoint is at `/infer`. For example:

    $ curl -d '{"texts": ["hi", "hello", "how are you"]}' https://localhost:8080/infer
    ["Can't Decide", "Not Relevant", "Relevant"]

API doc is available at `/api/doc` (web interface) and `/api/doc/swagger.json` (swagger json),
A generated swagger json, converted to yaml, is also available in the repository.

## Server design

This server is designed using a side-car pattern, the actual model inference task is handled by
[TensorFlow Serving](https://www.tensorflow.org/tfx/guide/serving).
And an [aiohttp](https://aiohttp.readthedocs.io/) server acts as api docs hosting, reverse proxy and input validation.

Also to run everything inside the same container for CloudRun deployment, [Supervisord](http://supervisord.org/) is
used to manage processes. But if the deployment is based on kubernetes, the tf serving and reverse proxy should be
different containers.
