#!/bin/bash

gcloud --project=${GOOGLE_CLOUD_PROJECT} run services --region=${GOOGLE_CLOUD_REGION} --platform=managed update ${K_SERVICE} --memory 1024
