import logging
import os
from typing import List

import aiohttp
from aiohttp import web
from aiohttp_swagger import setup_swagger


logger = logging.getLogger('webserver')


async def root(request):
    """
    description: This end-point allow to test that service is up.
    tags:
    - Health check
    produces:
    - text/plain
    responses:
        "200":
            description: the server is up
    """
    return web.Response(text="aloha!")


def validate_infer_input(payload):
    if not isinstance(payload, dict):
        raise ValueError('payload is not a dict')
    if 'texts' not in payload:
        raise ValueError('\'texts\' field not exist')
    if not isinstance(payload['texts'], list):
        raise ValueError('payload[\'texts\'] is not a dict')
    for t in payload['texts']:
        if not isinstance(t, str):
            raise ValueError(f'element "{t}" is not a string')

    return True


async def tfserving_infer(url, inputs: List[str]):
    req_payload = {
        'inputs': inputs
    }
    result = []
    async with aiohttp.ClientSession() as client:
        async with client.request('post', url, json=req_payload) as resp:
            if resp.status != 200:
                resp_data = await resp.json()
                raise RuntimeError(f'inference error: {resp_data}')

            resp_data = await resp.json()

    # sqlite> select choose_one, class_label from data group by choose_one, class_label;
    # Can't Decide|2
    # Not Relevant|0
    # Relevant|1

    for output in resp_data['outputs']:
        argmax = max(range(len(output)), key=lambda i: output[i])
        if argmax == 0:
            result.append('Not Relevant')
        elif argmax == 1:
            result.append('Relevant')
        else:
            result.append('Can\'t Decide')
    return result


async def infer(request):
    """
    description: Infer given texts
    tags:
    - Inference
    parameters:
    - in: body
      name: input
      description: The texts to be inferred
      required: true
      schema:
        type: object
        properties:
          texts:
            type: array
            items:
              type: string
    produces:
    - application/json
    responses:
        "200":
            description: the inference result
            schema:
                type: array
                items:
                    type: string
        "400":
            description: bad input
        "500":
            description: general inference error
    """
    request_body = await request.json()

    try:
        validate_infer_input(request_body)
    except Exception as e:
        return web.HTTPBadRequest(reason=str(e))

    try:
        result = await tfserving_infer(request.app['tfserving_url'], request_body['texts'])
    except Exception as e:
        return web.HTTPInternalServerError(reason=str(e))

    return web.json_response(result)


def build_app(tfserving_url_prefix: str = 'http://localhost:8501/v1/models/',
              model_name: str = 'example_model'):
    if os.environ.get('MODEL_NAME', None):
        model_name = os.environ['MODEL_NAME']

    app = web.Application()
    app['tfserving_url'] = f'{tfserving_url_prefix}{model_name}:predict'

    app.router.add_route('GET', '/', root)
    app.router.add_route('POST', '/infer', infer)

    setup_swagger(app,
                  description="Model server serving models for the Grab DS assignment",
                  title="Grab DS Model Server",
                  api_version="0.1.0")
    return app


async def get_app():
    return build_app()


def cli(host='0.0.0.0', port=8080,
        tfserving_url_prefix='http://localhost:8501/v1/models/', model_name='example_model'):
    app = build_app(tfserving_url_prefix=tfserving_url_prefix, model_name=model_name)
    web.run_app(app, host=host, port=port)


if __name__ == '__main__':
    import coloredlogs

    coloredlogs.install(level='DEBUG')

    from fire import Fire
    Fire(cli)
