#!/bin/bash -xe

IMAGE=${IMAGE:-grabds-serveing}
MODEL_NAME=${MODEL_NAME:-example_model}

docker build -t ${IMAGE} --build-arg MODEL_NAME=MODEL_NAME .
