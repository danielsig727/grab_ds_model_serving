#!/bin/bash

TF_SERVING_PORT=${TF_SERVING_PORT:-8500}
TF_SERVING_REST_PORT=${TF_SERVING_REST_PORT:-8501}

tensorflow_model_server --port=${TF_SERVING_PORT} \
                        --rest_api_port=${TF_SERVING_REST_PORT} \
                        --model_name=${MODEL_NAME} \
                        --model_base_path=${MODEL_BASE_PATH}/${MODEL_NAME} \
                        "$@"
