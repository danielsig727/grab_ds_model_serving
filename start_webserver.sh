#!/bin/bash -xe

HOST=${HOST:-0.0.0.0}
PORT=${PORT:-8080}
WROKERS=${WROKERS:-2}

gunicorn grabds_serving.webserver:get_app --bind ${HOST}:${PORT} \
                                          --workers ${WROKERS} \
                                          --access-logfile - \
                                          --worker-class aiohttp.GunicornWebWorker
